#ifndef _BONOBO_UI_NODE_PRIVATE_H_
#define _BONOBO_UI_NODE_PRIVATE_H_

/* All this for xmlChar, xmlStrdup !? */
#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>
#include <gnome-xml/xmlmemory.h>

#include <bonobo/bonobo-ui-node.h>

struct _BonoboUINode {
	/* Tree management */
	BonoboUINode *parent;
	BonoboUINode *children;
	BonoboUINode *prev;
	BonoboUINode *next;

	/* The useful bits */
	guint32       name_id;
	xmlChar      *content;
	GArray       *attrs;
	gpointer      user_data;
};

typedef struct {
	guint32       id;
	xmlChar      *value;
} BonoboUIAttr;

guint32     bonobo_ui_node_get_id         (const char   *str);
const char *bonobo_ui_node_str_from_id    (guint32       id);

void        bonobo_ui_node_set_attr_by_id (BonoboUINode *node,
					   guint32       id,
					   const char   *value);
const char *bonobo_ui_node_get_attr_by_id (BonoboUINode *node,
					   guint32       id);
const char *bonobo_ui_node_peek_content   (BonoboUINode *node);
gboolean    bonobo_ui_node_has_name_by_id (BonoboUINode *node,
					   guint32       id);
void        bonobo_ui_node_add_after      (BonoboUINode *before,
					   BonoboUINode *new_after);
void        bonobo_ui_node_move_children  (BonoboUINode *from,
					   BonoboUINode *to);

#endif /* _BONOBO_UI_NODE_PRIVATE_H_ */
