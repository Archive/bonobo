#!/usr/bin/perl

sub id_trans {
    ($goad_id) = @_; 
    
    if (! $goad_to_oaf{$goad_id}) {
	$goad_to_oaf{$goad_id} = "OAFIID:$goad_id:" . `uuidgen`;
	chomp $goad_to_oaf{$goad_id};
    }

    return $goad_to_oaf{$goad_id};
}


sub printserver () {
    if ($have_server_record) {
	$new_id = id_trans "${server_record{goadid}}";
	$new_location = "${server_record{location_info}}";
	$type = "${server_record{type}}";
	
	if ($type eq "factory") {
	    $new_location = id_trans $new_location;
	}

	print OUTFILE "<oaf_server iid=\"${new_id}\"\n",
	              "            type=\"${type}\"\n",
		      "            location=\"${new_location}\">\n\n";

	if ($server_record{repo_id}) {
	    print OUTFILE "\t<oaf_attribute name=\"repo_ids\" type=\"stringv\">\n";
	    $_ = $server_record{"repo_id"};
	    @repo_ids = split (/[ \t]+/);

	    foreach $id (@repo_ids) {
		print OUTFILE "\t\t<item value=\"${id}\"/>\n";
	    }
	    print OUTFILE "\t</oaf_attribute>\n\n";
	}


	if ($server_record{description}) {
	    print OUTFILE "\t<oaf_attribute name=\"description\" type=\"string\"\n",
	                  "\t               value=\"${server_record{description}}\"/>\n";
	}

	print OUTFILE "\n</oaf_server>\n\n";
    }
}

for $file (@ARGV) {
    $have_server_record = 0;
    $server_record{"description"} = "";
    $server_record{"repo_id"} = "";
    $server_record{"location_info"} = "";
    $server_record{"type"} = "";

    open INFILE, "<$file";
    $out = $file;
    $out =~ s/.(gnorba|goad)/.oafinfo/;
    open OUTFILE, ">$out";

    print "XXX - Writing to $out\n";

    print OUTFILE "<oaf_info>\n\n";

    while (<INFILE>) {
	chomp;
	if (/^\[.*\]/) {
	    # output old server info
	    $foo = $_;
	    printserver;
	    $_ = $foo;
	    
	    # begin new server record
	    s/\[|\]//g;
	    $server_record{"goadid"} = $_;
	    
	    $have_server_record = 1;
	    $server_record{"description"} = "";
	    $server_record{"repo_id"} = "";
	    $server_record{"location_info"} = "";
	    $server_record{"type"} = "";
	    
	} elsif  (/.*=.*/) {
	    $name = $_;
	    $value = $_;
	    
	    $name =~ s/=.*//;
	$value =~ s/.*=//;
	    
	    # print "${name}=${value}\n";
	    
	    $server_record{$name}=$value;
	}
	
	
    }
    
    printserver;
    
    
    print OUTFILE "</oaf_info>\n";

    close INFILE;
    close OUTFILE;

}



