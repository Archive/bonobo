#!/usr/bin/perl -pi.bak

     # BonoboWin -> BonoboWindow
	s/BonoboWin/BonoboWindow/g;
	s/bonobo_win_/bonobo_window_/g;
	s/BONOBO_WIN([^D])/BONOBO_WINDOW$1/g;
	s/BONOBO_TYPE_WIN/BONOBO_TYPE_WINDOW/g;

	# Object directory ugliness
	s/od_get_orb/bonobo_directory_get_orb/g;
	s/od_server_info_new/bonobo_directory_new_server_info/g;
	s/od_server_info_get/bonobo_directory_get_server_info/g;
	s/od_server_info_ref/bonobo_directory_server_info_ref/g;
	s/od_server_info_unref/bonobo_directory_server_info_unref/g;

	s/od_get_server_list/bonobo_directory_get_server_list/g;
	s/od_server_list_free/bonobo_directory_free_server_list/g;
	s/od_server_activate_with_iid/bonobo_directory_activate_from_iid/g;
	s/od_server_register/bonobo_directory_register_server/g;
	s/od_server_unregister/bonobo_directory_unregister_server/g;
	s/od_name_service_get/bonobo_directory_get_name_service/g;

#
# Guidelines: cf. http://java.sun.com/docs/books/jls/html/6.doc.html#11186
#
# Method names should be verbs or verb phrases, in mixed case, with
# the first letter lowercase and the first letter of any subsequent
# words capitalized. Here are some additional specific conventions for
# method names:
#
# Methods to get and set an attribute that might be thought of as a
# variable V should be named getV and setV. An example is the methods
# getPriority (�20.20.22) and setPriority (�20.20.23) of class
# java.lang.Thread.
#
#       A method that returns the length of something should be named
#       length, as in class java.lang.String (�20.12.11).
#
#       A method that tests a boolean condition V about an object
#       should be named isV. An example is the method isInterrupted of
#       class java.lang.Thread (�20.20.32).
#
#       A method that converts its object to a particular format F
#       should be named toF. Examples are the method toString of class
#       java.lang.Object (�20.1.2) and the methods toLocaleString
#       (�21.3.27) and toGMTString (�21.3.28) of class java.util.Date.
#
# Whenever possible and appropriate, basing the names of methods in a
# new class on names in an existing class that is similar, especially
# a class from the standard Java Application Programming Interface
# classes, will make it easier to use.

	#stdlycaps

    s/Bonobo_AdviseSink_on_rename/Bonobo_AdviseSink_notifyRename/g;

    s/Bonobo_Canvas_Component_canvas_size_set/Bonobo_Canvas_Component_setCanvasSize/g;
    s/Bonobo_Canvas_Component_set_bounds/Bonobo_Canvas_Component_setBounds/g;

    s/Bonobo_Canvas_ComponentProxy_request_update/Bonobo_Canvas_ComponentProxy_updateArea/g;

    s/Bonobo_ClientSite_get_container/Bonobo_ClientSite_getContainer/g;
    s/Bonobo_ClientSite_show_window/Bonobo_ClientSite_showWindow/g;
    s/Bonobo_ClientSite_save_object/Bonobo_ClientSite_saveObject/g;

    s/Bonobo_ControlFrame_get_ambient_properties/Bonobo_ControlFrame_getAmbientProperties/g;
    s/Bonobo_ControlFrame_queue_resize/Bonobo_ControlFrame_queueResize/g;
    s/Bonobo_ControlFrame_activate_uri/Bonobo_ControlFrame_activateURI/g;
    s/Bonobo_ControlFrame_get_ui_handler/Bonobo_ControlFrame_getUIHandler/g;
    s/Bonobo_ViewFrame_get_ui_handler/Bonobo_ViewFrame_getUIHandler/g;

    s/Bonobo_View_set_zoom_factor/Bonobo_View_setZoomFactor/g;
    s/Bonobo_ViewFrame_get_client_site/Bonobo_ViewFrame_getClientSite/g;
    s/Bonobo_Embeddable_set_client_site/Bonobo_Embeddable_setClientSite/g;
    s/Bonobo_Embeddable_get_client_site/Bonobo_Embeddable_getClientSite/g;
    s/Bonobo_Embeddable_set_host_name/Bonobo_Embeddable_setHostName/g;
    s/Bonobo_Embeddable_set_uri/Bonobo_Embeddable_setURI/g;
    s/Bonobo_Embeddable_get_misc_status/Bonobo_Embeddable_getMiscStatus/g;
    s/Bonobo_Embeddable_new_view/Bonobo_Embeddable_createView/g;
    s/Bonobo_Embeddable_new_canvas_item/Bonobo_Embeddable_createCanvasItem/g;

    s/Bonobo_Control_set_frame/Bonobo_Control_setFrame/g;
    s/Bonobo_Control_set_window/Bonobo_Control_setWindowId/g;
    s/Bonobo_Control_get_property_bag/Bonobo_Control_getProperties/g;
    s/Bonobo_Control_size_allocate/Bonobo_Control_setSize/g;
    s/Bonobo_Control_size_request/Bonobo_Control_getDesiredSize/g;
    s/Bonobo_Control_set_state/Bonobo_Control_setState/g;
    s/bonobo_control_get_property_bag/bonobo_control_get_properties/g;
    s/bonobo_control_set_property_bag/bonobo_control_set_properties/g;

    s/Bonobo_PropertyControl_get_control/Bonobo_PropertyControl_getControl/g;

    s/Bonobo_Desktop_Window_get_geometry/Bonobo_Desktop_Window_getGeometry/g;
    s/Bonobo_Desktop_Window_set_geometry/Bonobo_Desktop_Window_setGeometry/g;
    s/Bonobo_Desktop_Window_get_window_id/Bonobo_Desktop_Window_getWindowId/g;

    s/Bonobo_ItemContainer_enum_objects/Bonobo_ItemContainer_enumObjects/g;
    s/Bonobo_ItemContainer_get_object/Bonobo_ItemContainer_getObjectByName/g;

    s/Bonobo_EventSource_add_listener/Bonobo_EventSource_addListener/g;
    s/Bonobo_EventSource_remove_listener/Bonobo_EventSource_removeListener/g;

    s/Bonobo_Moniker_get_display_name/Bonobo_Moniker_getDisplayName/g;
    s/Bonobo_Moniker_parse_display_name/Bonobo_Moniker_parseDisplayName/g;

    s/Bonobo_Persist_get_content_types/Bonobo_Persist_getContentTypes/g;
    s/Bonobo_PersistFile_is_dirty/Bonobo_PersistFile_isDirty/g;
    s/Bonobo_PersistFile_get_current_file/Bonobo_PersistFile_getCurrentFile/g;

    s/Bonobo_PersistStorage_is_dirty/Bonobo_PersistStorage_isDirty/g;
    s/Bonobo_PersistStorage_init_new/Bonobo_PersistStorage_initNew/g;

    s/Bonobo_PersistStream_is_dirty/Bonobo_PersistStream_isDirty/g;
    s/Bonobo_PersistStream_get_size_max/Bonobo_PersistStream_getMaxSize/g;

    s/Bonobo_ProgressiveDataSink_add_data/Bonobo_ProgressiveDataSink_addData/g;
    s/Bonobo_ProgressiveDataSink_set_size/Bonobo_ProgressiveDataSink_setSize/g;

    s/Bonobo_Property_get_name/Bonobo_Property_getName/g;
    s/Bonobo_Property_get_type/Bonobo_Property_getType/g;
    s/Bonobo_Property_set_value/Bonobo_Property_setValue/g;
    s/Bonobo_Property_get_value/Bonobo_Property_getValue/g;
    s/Bonobo_Property_get_default/Bonobo_Property_getDefault/g;
    s/Bonobo_Property_get_doc_string/Bonobo_Property_getDocString/g;
    s/Bonobo_Property_get_flags/Bonobo_Property_getFlags/g;

    s/Bonobo_PropertyBag_get_property/Bonobo_PropertyBag_getPropertyByName/g;
    s/Bonobo_PropertyBag_get_properties/Bonobo_PropertyBag_getProperties/g;
    s/Bonobo_PropertyBag_get_property_names/Bonobo_PropertyBag_getPropertyNames/g;
    s/Bonobo_PropertyBag_add_change_listener/Bonobo_PropertyBag_addChangeListener/g;
    s/Bonobo_PropertyBag_remove_change_listener/Bonobo_PropertyBag_removeChangeListener/g;
    s/Bonobo_PropertyBag_set_values/Bonobo_PropertyBag_setValues/g;
    s/Bonobo_PropertyBag_get_values/Bonobo_PropertyBag_getValues/g;

    s/Bonobo_Stream_get_info/Bonobo_Stream_getInfo/g;
    s/Bonobo_Stream_set_info/Bonobo_Stream_setInfo/g;
    s/Bonobo_Stream_copy_to/Bonobo_Stream_copyTo/g;

    s/Bonobo_Storage_get_info/Bonobo_Storage_getInfo/g;
    s/Bonobo_Storage_set_info/Bonobo_Storage_setInfo/g;
    s/Bonobo_Storage_open_stream/Bonobo_Storage_openStream/g;
    s/Bonobo_Storage_open_storage/Bonobo_Storage_openStorage/g;
    s/Bonobo_Storage_copy_to/Bonobo_Storage_copyTo/g;
    s/Bonobo_Storage_list_contents/Bonobo_Storage_listContents/g;
    s/Bonobo_Storage_open_stream/Bonobo_Storage_openStream/g;

    s/Bonobo_UIComponent_set_container/Bonobo_UIComponent_setContainer/g;
    s/Bonobo_UIComponent_unset_container/Bonobo_UIComponent_unsetContainer/g;
    s/Bonobo_UIComponent_describe_verbs/Bonobo_UIComponent_describeVerbs/g;
    s/Bonobo_UIComponent_exec_verb/Bonobo_UIComponent_execVerb/g;
    s/Bonobo_UIComponent_ui_event/Bonobo_UIComponent_uiEvent/g;

    s/Bonobo_UIContainer_register_component/Bonobo_UIContainer_registerComponent/g;
    s/Bonobo_UIContainer_deregister_component/Bonobo_UIContainer_deregisterComponent/g;
    s/Bonobo_UIContainer_node_set/Bonobo_UIContainer_setNode/g;
    s/Bonobo_UIContainer_node_get/Bonobo_UIContainer_getNode/g;
    s/Bonobo_UIContainer_node_remove/Bonobo_UIContainer_removeNode/g;
    s/Bonobo_UIContainer_node_exists/Bonobo_UIContainer_exists/g;
    s/Bonobo_UIContainer_object_set/Bonobo_UIContainer_setObject/g;
    s/Bonobo_UIContainer_object_get/Bonobo_UIContainer_getObject/g;

    s/Bonobo_Unknown_query_interface/Bonobo_Unknown_queryInterface/g;
    s/Bonobo_Control_query_interface/Bonobo_Control_queryInterface/g;

    s/Bonobo_ZoomableFrame_report_zoom_level_changed/Bonobo_ZoomableFrame_onLevelChanged/g;
    s/Bonobo_ZoomableFrame_report_zoom_parameters_changed/Bonobo_ZoomableFrame_onParametersChanged/g;

# attributes need extra '_'s & _set /g; _gets
    s/Bonobo_Zoomable__get_min_zoom_level/Bonobo_Zoomable__get_minLevel/g;
    s/Bonobo_Zoomable__get_max_zoom_level/Bonobo_Zoomable__get_maxLevel/g;
    s/Bonobo_Zoomable__get_has_min_zoom_level/Bonobo_Zoomable__get_hasMinLevel/g;
    s/Bonobo_Zoomable__get_has_max_zoom_level/Bonobo_Zoomable__get_hasMaxLevel/g;
    s/Bonobo_Zoomable__get_is_continuous/Bonobo_Zoomable__get_isContinuous/g;
    s/Bonobo_Zoomable__get_preferred_zoom_levels/Bonobo_Zoomable__get_preferredLevels/g;
    s/Bonobo_Zoomable__get_preferred_zoom_level_names/Bonobo_Zoomable__get_preferredLevelNames/g;

    s/Bonobo_Zoomable_zoom_in/Bonobo_Zoomable_zoomIn/g;
    s/Bonobo_Zoomable_zoom_out/Bonobo_Zoomable_zoomOut/g;
    s/Bonobo_Zoomable_zoom_to_fit/Bonobo_Zoomable_zoomFit/g;
    s/Bonobo_Zoomable_zoom_to_default/Bonobo_Zoomable_zoomDefault/g;
    s/Bonobo_Zoomable_set_zoom_level/Bonobo_Zoomable_setLevel/g;
    s/Bonobo_Zoomable_set_frame/Bonobo_Zoomable_setFrame/g;
