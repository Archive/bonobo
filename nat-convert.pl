#!/usr/bin/perl -pi.bak

     # This does View and ViewFrame
     s/GNOME_VIEW/BONOBO_VIEW/g;
     s/GNOME_IS_VIEW/BONOBO_IS_VIEW/g;
     s/GnomeView/BonoboView/g;
     s/gnome_view/bonobo_view/g;
     s/GNOME_View/Bonobo_View/g;
     s/GNOME::View/Bonobo::View/g;
     s/GNOMEView/BonoboView/g;

     s/GNOME_OBJECT/BONOBO_OBJECT/g;
     s/GNOME_IS_OBJECT/BONOBO_IS_OBJECT/g;
     s/GnomeObject/BonoboObject/g;
     s/gnome_object/bonobo_object/g;
     s/GNOME_Unknown/Bonobo_Unknown/g;
     s/GNOME::Unknown/Bonobo::Unknown/g;

     s/GNOME_BONOBO_WIDGET/BONOBO_WIDGET/g;
     s/GNOME_IS_BONOBO_WIDGET/BONOBO_IS_WIDGET/g;
     s/GnomeBonoboWidget/BonoboWidget/g;
     s/gnome_bonobo_widget/bonobo_widget/g;

     s/GNOME_CLIENT_SITE/BONOBO_CLIENT_SITE/g;
     s/GNOME_IS_CLIENT_SITE/BONOBO_IS_CLIENT_SITE/g;
     s/GnomeClientSite/BonoboClientSite/g;
     s/gnome_client_site/bonobo_client_site/g;
     s/GNOME_ClientSite/Bonobo_ClientSite/g;
     s/GNOME::ClientSite/Bonobo::ClientSite/g;
     s/GNOMEClientSite/BonoboClientSite/g;

     s/GNOME_CONTAINER/BONOBO_CONTAINER/g;
     s/GNOME_IS_CONTAINER/BONOBO_IS_CONTAINER/g;
     s/GnomeContainer/BonoboContainer/g;
     s/gnome_container/bonobo_container/g;
     s/GNOME_Container/Bonobo_Container/g;
     s/GNOME::Container/Bonobo::Container/g;
     s/GNOMEContainer/BonoboContainer/g;

     s/GNOME_EMBEDDABLE/BONOBO_EMBEDDABLE/g;
     s/GNOME_IS_EMBEDDABLE/BONOBO_IS_EMBEDDABLE/g;
     s/GnomeEmbeddable/BonoboEmbeddable/g;
     s/gnome_embeddable/bonobo_embeddable/g;
     s/GNOME_Embeddable/Bonobo_Embeddable/g;
     s/GNOME::Embeddable/Bonobo::Embeddable/g;
     s/GNOMEEMbeddable/BonoboEmbeddable/g;
     s/GNOME::Advise/Bonobo::Advise/g;
     s/GNOME_Advise/Bonobo_Advise/g;

     # Control and ControlFrame
     s/GNOME_CONTROL/BONOBO_CONTROL/g;
     s/GNOME_IS_CONTROL/BONOBO_IS_CONTROL/g;
     s/GnomeControl/BonoboControl/g;
     s/gnome_control/bonobo_control/g;
     s/GNOME_Control/Bonobo_Control/g;
     s/GNOME::Control/Bonobo::Control/g;
     s/GNOMEControl/BonoboControl/g;

     s/GNOME_MONIKER/BONOBO_MONIKER/g;
     s/GNOME_IS_MONIKER/BONOBO_IS_MONIKER/g;
     s/GnomeMoniker/BonoboMoniker/g;
     s/gnome_moniker/bonobo_moniker/g;
     s/GNOME_Moniker/Bonobo_Moniker/g;
     s/GNOME_BindContext/Bonobo_BindContext/g;
     s/GNOME::Moniker/Bonobo::Moniker/g;
     s/GNOMEMoniker/BonoboMoniker/g;

     s/GNOME_STREAM/BONOBO_STREAM/g;
     s/GNOME_IS_STREAM/BONOBO_IS_STREAM/g;
     s/GnomeStream/BonoboStream/g;
     s/gnome_stream/bonobo_stream/g;
     s/GNOME_Stream/Bonobo_Stream/g;
     s/GNOME::Stream/Bonobo::Stream/g;
     s/GNOMEStream/BonoboStream/g;

     s/GNOME_STORAGE/BONOBO_STORAGE/g;
     s/GNOME_IS_STORAGE/BONOBO_IS_STORAGE/g;
     s/GnomeStorage/BonoboStorage/g;
     s/gnome_storage/bonobo_storage/g;
     s/GNOME_STORAGE_READ/BONOBO_STORAGE_READ/g;
     s/GNOME_Storage/Bonobo_Storage/g;
     s/GNOME_SS/BONOBO_SS/g;

     s/GNOME_PERSIST/BONOBO_PERSIST/g;
     s/GNOME_IS_PERSIST/BONOBO_IS_PERSIST/g;
     s/GnomePersist/BonoboPersist/g;
     s/gnome_persist/bonobo_persist/g;
     s/GNOME_Persist/Bonobo_Persist/g;
     s/GNOME::Persist/Bonobo::Persist/g;
     s/GNOMEPersist/BonoboPersist/g;

     s/GNOME_PROGRESSIVE/BONOBO_PROGRESSIVE/g;
     s/GNOME_IS_PROGRESSIVE/BONOBO_IS_PROGRESSIVE/g;
     s/GnomeProgressive/BonoboProgressive/g;
     s/gnome_progressive/bonobo_progressive/g;
     s/GNOME_Progressive/Bonobo_Progressive/g;
     s/GNOME::Progressive/Bonobo::Progressive/g;
     s/GNOMEProgressive/BonoboProgressive/g;

     s/GNOME_PROPERTY_BAG/BONOBO_PROPERTY_BAG/g;
     s/GNOME_IS_PROPERTY_BAG/BONOBO_IS_PROPERTY_BAG/g;
     s/GnomePropertyBag/BonoboPropertyBag/g;
     s/gnome_property_bag/bonobo_property_bag/g;
     s/GNOME_PropertyBag/Bonobo_PropertyBag/g;
     s/GNOME::PropertyBag/Bonobo::PropertyBag/g;
     s/GNOMEPropertyBag/BonoboPropertyBag/g;

     s/GNOME_Property/Bonobo_Property/g;
     s/GNOME::Property/Bonobo::Property/g;
     s/GNOMEProperty/BonoboProperty/g;

     s/GNOME_UI_HANDLER/BONOBO_UI_HANDLER/g;
     s/GNOME_IS_UI_HANDLER/BONOBO_IS_UI_HANDLER/g;
     s/GnomeUIHandler/BonoboUIHandler/g;
     s/gnome_ui_handler/bonobo_ui_handler/g;
     s/GNOME_UIHandler/Bonobo_UIHandler/g;
     s/GNOME::UIHandler/Bonobo::UIHandler/g;
     s/GNOMEUIHandler/BonoboUIHandler/g;

     s/GNOME_WRAPPER/BONOBO_WRAPPER/g;
     s/GNOME_IS_WRAPPER/BONOBO_IS_WRAPPER/g;
     s/GnomeWrapper/BonoboWrapper/g;
     s/gnome_wrapper/bonobo_wrapper/g;

     s/GNOME_BONOBO_ITEM/BONOBO_CANVAS_ITEM/g;
     s/GNOME_IS_BONOBO_ITEM/BONOBO_IS_CANVAS_ITEM/g;
     s/GnomeBonoboItem/BonoboCanvasItem/g;
     s/gnome_bonobo_item/bonobo_canvas_item/g;

     s/GNOME_CANVAS_COMPONENT/BONOBO_CANVAS_COMPONENT/g;
     s/GNOME_IS_CANVAS_COMPONENT/BONOBO_IS_CANVAS_COMPONENT/g;
     s/GnomeCanvasComponent/BonoboCanvasComponent/g;
     s/gnome_canvas_component/bonobo_canvas_component/g;
     s/GNOMECanvasComponent/BonoboCanvasComponent/g;
     s/GNOME_Canvas_Item/Bonobo_Canvas_Component/g;
     s/GNOME::Gdk/Bonobo::Gdk/g;
     s/GNOME_Gdk/Bonobo_Gdk/g;
     s/GNOME_Canvas/Bonobo_Canvas/g;
     s/ItemProxy/ComponentProxy/g;

     s/GNOME_DESKTOP_WINDOW/BONOBO_DESKTOP_WINDOW/g;
     s/GNOME_IS_DESKTOP_WINDOW/BONOBO_IS_DESKTOP_WINDOW/g;
     s/GnomeDesktopWindow/BonoboDesktopWindow/g;
     s/gnome_desktop_window/bonobo_desktop_window/g;
     s/GNOME_Desktop/Bonobo_Desktop/g;
     s/GNOME::Desktop/Bonobo::Desktop/g;
     s/GNOMEDesktop/BonoboDesktop/g;

     s/GNOME_GENERIC_FACTORY/BONOBO_GENERIC_FACTORY/g;
     s/GNOME_IS_GENERIC_FACTORY/BONOBO_IS_GENERIC_FACTORY/g;
     s/GnomeGenericFactory/BonoboGenericFactory/g;
     s/gnome_generic_factory/bonobo_generic_factory/g;
     s/GNOME_Generic/Bonobo_Generic/g;
     s/GNOME::Generic/Bonobo::Generic/g;
     s/GNOMEGeneric/BonoboGeneric/g;

     s/GNOME_BONOBO_SELECTOR/BONOBO_SELECTOR/g;
     s/GnomeBonoboSelector/BonoboSelector/g;
     s/gnome_bonobo_selector/bonobo_selector/g;

     # header names
     s/gnome-bonobo-item.h/bonobo-canvas-item.h/g;
     s/gnome-bind-context.h/bonobo-bind-context.h/g;
     s/gnome-canvas-component.h/bonobo-canvas-component.h/g;
     s/gnome-canvas-item.h/bonobo-canvas-item.h/g;
     s/gnome-client-site.h/bonobo-client-site.h/g;
     s/gnome-component-directory.h/bonobo-component-directory.h/g;
     s/gnome-component-io.h/bonobo-component-io.h/g;
     s/gnome-composite-moniker.h/bonobo-composite-moniker.h/g;
     s/gnome-container.h/bonobo-container.h/g;
     s/gnome-control-frame.h/bonobo-control-frame.h/g;
     s/gnome-control.h/bonobo-control.h/g;
     s/gnome-desktop-window.h/bonobo-desktop-window.h/g;
     s/gnome-embeddable-factory.h/bonobo-embeddable-factory.h/g;
     s/gnome-embeddable.h/bonobo-embeddable.h/g;
     s/gnome-file-moniker.h/bonobo-file-moniker.h/g;
     s/gnome-generic-factory.h/bonobo-generic-factory.h/g;
     s/gnome-item-moniker.h/bonobo-item-moniker.h/g;
     s/gnome-local-property-bag.h/bonobo-local-property-bag.h/g;
     s/gnome-main.h/bonobo-main.h/g;
     s/gnome-moniker-client.h/bonobo-moniker-client.h/g;
     s/gnome-moniker.h/bonobo-moniker.h/g;
     s/gnome-object-client.h/bonobo-object-client.h/g;
     s/gnome-object-io.h/bonobo-object-io.h/g;
     s/gnome-object.h/bonobo-object.h/g;
     s/gnome-persist-file.h/bonobo-persist-file.h/g;
     s/gnome-persist-stream.h/bonobo-persist-stream.h/g;
     s/gnome-persist.h/bonobo-persist.h/g;
     s/gnome-progressive.h/bonobo-progressive.h/g;
     s/gnome-property-bag-client.h/bonobo-property-bag-client.h/g;
     s/gnome-property-bag-xml.h/bonobo-property-bag-xml.h/g;
     s/gnome-property-bag.h/bonobo-property-bag.h/g;
     s/gnome-property-types.h/bonobo-property-types.h/g;
     s/gnome-property.h/bonobo-property.h/g;
     s/gnome-bonobo-selector.h/bonobo-selector.h/g;
     s/gnome-simple-source.h/bonobo-simple-source.h/g;
     s/gnome-storage-driver.h/bonobo-storage-driver.h/g;
     s/gnome-storage-priv.h/bonobo-storage-priv.h/g;
     s/gnome-storage-private.h/bonobo-storage-private.h/g;
     s/gnome-storage.h/bonobo-storage.h/g;
     s/gnome-stream-client.h/bonobo-stream-client.h/g;
     s/gnome-stream-fs.h/bonobo-stream-fs.h/g;
     s/gnome-stream-memory.h/bonobo-stream-memory.h/g;
     s/gnome-stream.h/bonobo-stream.h/g;
     s/gnome-ui-handler.h/bonobo-ui-handler.h/g;
     s/gnome-view-frame.h/bonobo-view-frame.h/g;
     s/gnome-view.h/bonobo-view.h/g;
     s/gnome-widget.h/bonobo-widget.h/g;
     s/gnome-wrapper.h/bonobo-wrapper.h/g;
     s/gnome-property-client.h/gnome-property-client.h/g;
     s/bonobo\/gnome-bonobo.h/bonobo.h/g;
     s/bonobo.idl/Bonobo.idl/g;

     # .c names
     s/gnome-bonobo-item.c/bonobo-canvas-item.c/g;
     s/gnome-bind-context.c/bonobo-bind-context.c/g;
     s/gnome-canvas-component.c/bonobo-canvas-component.c/g;
     s/gnome-canvas-item.c/bonobo-canvas-item.c/g;
     s/gnome-client-site.c/bonobo-client-site.c/g;
     s/gnome-component-directory.c/bonobo-component-directory.c/g;
     s/gnome-component-io.c/bonobo-component-io.c/g;
     s/gnome-composite-moniker.c/bonobo-composite-moniker.c/g;
     s/gnome-container.c/bonobo-container.c/g;
     s/gnome-control-frame.c/bonobo-control-frame.c/g;
     s/gnome-control.c/bonobo-control.c/g;
     s/gnome-desktop-window.c/bonobo-desktop-window.c/g;
     s/gnome-embeddable-factory.c/bonobo-embeddable-factory.c/g;
     s/gnome-embeddable.c/bonobo-embeddable.c/g;
     s/gnome-file-moniker.c/bonobo-file-moniker.c/g;
     s/gnome-generic-factory.c/bonobo-generic-factory.c/g;
     s/gnome-item-moniker.c/bonobo-item-moniker.c/g;
     s/gnome-local-property-bag.c/bonobo-local-property-bag.c/g;
     s/gnome-main.c/bonobo-main.c/g;
     s/gnome-moniker-client.c/bonobo-moniker-client.c/g;
     s/gnome-moniker.c/bonobo-moniker.c/g;
     s/gnome-object-client.c/bonobo-object-client.c/g;
     s/gnome-object-io.c/bonobo-object-io.c/g;
     s/gnome-object.c/bonobo-object.c/g;
     s/gnome-persist-file.c/bonobo-persist-file.c/g;
     s/gnome-persist-stream.c/bonobo-persist-stream.c/g;
     s/gnome-persist.c/bonobo-persist.c/g;
     s/gnome-progressive.c/bonobo-progressive.c/g;
     s/gnome-property-bag-client.c/bonobo-property-bag-client.c/g;
     s/gnome-property-bag-xml.c/bonobo-property-bag-xml.c/g;
     s/gnome-property-bag.c/bonobo-property-bag.c/g;
     s/gnome-property-types.c/bonobo-property-types.c/g;
     s/gnome-property.c/bonobo-property.c/g;
     s/gnome-bonobo-selector.c/bonobo-selector.c/g;
     s/gnome-simple-source.c/bonobo-simple-source.c/g;
     s/gnome-storage-driver.c/bonobo-storage-driver.c/g;
     s/gnome-storage-priv.c/bonobo-storage-priv.c/g;
     s/gnome-storage-private.c/bonobo-storage-private.c/g;
     s/gnome-storage.c/bonobo-storage.c/g;
     s/gnome-stream-client.c/bonobo-stream-client.c/g;
     s/gnome-stream-fs.c/bonobo-stream-fs.c/g;
     s/gnome-stream-memory.c/bonobo-stream-memory.c/g;
     s/gnome-stream.c/bonobo-stream.c/g;
     s/gnome-ui-handler.c/bonobo-ui-handler.c/g;
     s/gnome-view-frame.c/bonobo-view-frame.c/g;
     s/gnome-view.c/bonobo-view.c/g;
     s/gnome-widget.c/bonobo-widget.c/g;
     s/gnome-wrapper.c/bonobo-wrapper.c/g;
     s/gnome-property-client.c/gnome-property-client.c/g;
     s/bonobo\/gnome-bonobo.c/bonobo.c/g;

     s/\"view_activate\"/\"activate\"/g;
     s/\"view_activated\"/\"activated\"/g;
