/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef _BONOBO_MONIKER_UNTAR_H_
#define _BONOBO_MONIKER_UNTAR_H_

#include <bonobo/bonobo-stream.h>

BEGIN_GNOME_DECLS

#define BONOBO_MONIKER_UNTAR_TYPE        (bonobo_moniker_untar_get_type ())
#define BONOBO_MONIKER_UNTAR(o)          (GTK_CHECK_CAST ((o), BONOBO_MONIKER_UNTAR_TYPE, BonoboMonikerUntar))
#define BONOBO_MONIKER_UNTAR_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), BONOBO_MONIKER_UNTAR_TYPE, BonoboMonikerUntarClass))
#define BONOBO_IS_MONIKER_UNTAR(o)       (GTK_CHECK_TYPE ((o), BONOBO_MONIKER_UNTAR_TYPE))
#define BONOBO_IS_MONIKER_UNTAR_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), BONOBO_MONIKER_UNTAR_TYPE))

typedef struct _BonoboMonikerUntar BonoboMonikerUntar;

struct _BonoboMonikerUntar {
	BonoboMoniker stream;
};

typedef struct {
	BonoboMonikerClass parent_class;
} BonoboMonikerUntarClass;

GtkType        bonobo_moniker_untar_get_type (void);
BonoboMoniker *bonobo_moniker_untar_new      (void);
	
END_GNOME_DECLS

#endif /* _BONOBO_MONIKER_UNTAR_H_ */
