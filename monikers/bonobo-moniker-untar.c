/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * bonobo-moniker-untar.c: untarring Moniker
 *
 * Author:
 *   Vladimir Vukicevic (vladimir@helixcode.com)
 *
 * Copyright (C) 2000 Helix Code, Inc.
 */

/*
 *  TODO
 *  -- Do The Right Thing if a directory is requested (Storage interface only)
 *     this way, we can support file managers nicely.
 *  -- Cache
 */

#include <config.h>
#include <gnome.h>
#include <liboaf/liboaf.h>
#include <bonobo.h>
#include <bonobo/bonobo-stream-memory.h>
#include <bonobo/bonobo-tar-storage.h>
#include <zlib.h>

#include "bonobo-moniker-untar.h"

#define PREFIX_LEN (sizeof ("untar:") - 1)

static BonoboMonikerClass *bonobo_moniker_untar_parent_class;

static Bonobo_Unknown
untar_resolve (BonoboMoniker *moniker,
	      const Bonobo_ResolveOptions *options,
	      const CORBA_char *requested_interface,
	      CORBA_Environment *ev)
{
	const char *requested_fname;

	Bonobo_Moniker mparent;
	const char *mparent_name;

	Bonobo_Stream input_stream;
	Bonobo_PersistStream tar_stream;
	BonoboStorage *tar_storage;
	
	requested_fname = bonobo_moniker_get_name (moniker, PREFIX_LEN);
	fprintf (stderr, "requirested_fname: %s\n", requested_fname);
	/* The untar moniker can only operate on something that supports
	 * a stream; it has no meaning on its own
	 */
	mparent = bonobo_moniker_get_parent (moniker, ev);
	mparent_name = bonobo_moniker_client_get_name (mparent, ev);
	
	if (ev->_major != CORBA_NO_EXCEPTION)
		return CORBA_OBJECT_NIL;
	
	if (mparent == CORBA_OBJECT_NIL) {
		/* No parent! */
		g_warning ("untar moniker used with NIL parent");
		CORBA_exception_set
			(ev, CORBA_USER_EXCEPTION,
			 ex_Bonobo_Moniker_InvalidSyntax, NULL);
		return CORBA_OBJECT_NIL;
	}
	
	/* Try to grab the stream from the parent */
	input_stream = Bonobo_Moniker_resolve (mparent, options, "IDL:Bonobo/Stream:1.0", ev);
	if (ev->_major != CORBA_NO_EXCEPTION) {
		/* parent probably doesn't support stream */
		bonobo_object_release_unref (mparent, ev);
		return CORBA_OBJECT_NIL;
	}

	if (input_stream == CORBA_OBJECT_NIL) {
		g_warning ("untar moniker used with NIL parent");
		CORBA_exception_set
			(ev, CORBA_USER_EXCEPTION,
			 ex_Bonobo_Moniker_InvalidSyntax, NULL);
		bonobo_object_release_unref (mparent, ev);
		return CORBA_OBJECT_NIL;
	}

	/* It really ought to just query for something that can do this */
	tar_storage = bonobo_tar_storage_create ();
	if (!tar_storage) {
		bonobo_object_release_unref (input_stream, ev);
		bonobo_object_release_unref (mparent, ev);
		return CORBA_OBJECT_NIL;
	}

	tar_stream = Bonobo_Unknown_queryInterface 
		(BONOBO_OBJREF (tar_storage), "IDL:Bonobo/PersistStream:1.0", 
		 ev);
	if (ev->_major != CORBA_NO_EXCEPTION) {
		bonobo_object_release_unref (input_stream, ev);
		bonobo_object_release_unref (mparent, ev);
		return CORBA_OBJECT_NIL;
	}

	Bonobo_PersistStream_load (tar_stream, input_stream, "application/x-tar", ev);
	bonobo_object_release_unref (input_stream, ev);

	if (ev->_major != CORBA_NO_EXCEPTION) {
		bonobo_object_release_unref (tar_stream, ev);
		bonobo_object_release_unref (mparent, ev);
		return CORBA_OBJECT_NIL;
	}

	if (!strcmp (requested_interface, "IDL:Bonobo/Storage:1.0")) {
		if (requested_fname == NULL || *requested_fname == '\0') {
			/* This should select the directory first (i.e. the substorage) */
			g_warning ("untar moniker: storage requested, requested_fname: %s!",
				   requested_fname);
		}

		return bonobo_moniker_util_qi_return
			(CORBA_Object_duplicate (BONOBO_OBJREF (tar_storage),
						 ev),
			 requested_interface,
			 ev);
	} else if (!strcmp (requested_interface, "IDL:Bonobo/Stream:1.0")) {
		Bonobo_Stream stream_out;

		stream_out = Bonobo_Storage_openStream
			(BONOBO_OBJREF (tar_storage),
			 requested_fname,
			 Bonobo_Storage_READ,
			 ev);
		return stream_out;
	}

	return bonobo_moniker_use_extender ("OAFIID:Bonobo_MonikerExtender_stream",
					    moniker, options, requested_interface, ev);
}


static void
bonobo_moniker_untar_class_init (BonoboMonikerUntarClass *klass)
{
	BonoboMonikerClass *moniker_class = (BonoboMonikerClass *) klass;
	bonobo_moniker_untar_parent_class = gtk_type_class
		(bonobo_moniker_get_type ());

	moniker_class->resolve = untar_resolve;
}

GtkType
bonobo_moniker_untar_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		GtkTypeInfo info = {
			"BonoboMonikerUntar",
			sizeof(BonoboMonikerUntar),
			sizeof(BonoboMonikerUntarClass),
			(GtkClassInitFunc) bonobo_moniker_untar_class_init,
			(GtkObjectInitFunc) NULL,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (bonobo_moniker_get_type (), &info);
	}

	return type;
}

static BonoboMonikerUntar *
bonobo_moniker_untar_new (void)
{
	BonoboMoniker *moniker;
	Bonobo_Moniker corba_moniker;

	moniker = gtk_type_new (bonobo_moniker_untar_get_type ());
	corba_moniker = bonobo_moniker_corba_object_create
		(BONOBO_OBJECT (moniker));

	if (corba_moniker == CORBA_OBJECT_NIL) {
		bonobo_object_unref (BONOBO_OBJECT (moniker));
		return NULL;
	}

	if (!bonobo_object_construct (BONOBO_OBJECT (moniker), corba_moniker)) {
		bonobo_object_unref (BONOBO_OBJECT (moniker));
		return NULL;
	}

	return BONOBO_MONIKER_UNTAR (moniker);
}

static BonoboObject *
bonobo_moniker_untar_factory (BonoboGenericFactory *this,
			      void *unused)
{
	BonoboMonikerUntar *untar_moniker;

	untar_moniker = bonobo_moniker_untar_new ();

	return BONOBO_OBJECT (untar_moniker);
}

BONOBO_OAF_FACTORY ("OAFIID:Bonobo_Moniker_untar_Factory",
		    "untar-moniker", VERSION, 
		    bonobo_moniker_untar_factory,
		    NULL)
