/*
 * moniker-control-test.c: Little tester for moniker-resolved controls
 *
 * Author:
 *   Joe Shaw (joe@helixcode.com)
 *
 */

#include <config.h>
#include <gnome.h>
#include <liboaf/liboaf.h>
#include <bonobo.h>

#if 1 
static void
activate_clicked (GtkButton *button, GtkBox *box)
{
	char *url;
	CORBA_Environment ev;
	Bonobo_Control control;
	BonoboControlFrame *cf;
	Bonobo_PropertyBag bag;
	GtkWidget *widget;

	CORBA_exception_init (&ev);

	url = "http://www.helixcode.com";

	control = bonobo_get_object (url, "IDL:Bonobo/Control:1.0", &ev);

	widget = bonobo_widget_new_control_from_objref (
		control, CORBA_OBJECT_NIL);

	bag = Bonobo_Unknown_queryInterface (
		control, "IDL:Bonobo/PropertyBag:1.0", &ev);

#if 0	
	cf = bonobo_widget_get_control_frame (BONOBO_WIDGET (widget));
	bag = bonobo_control_frame_get_control_property_bag (cf, &ev);
#endif
	g_return_if_fail (ev._major == CORBA_NO_EXCEPTION);
	g_return_if_fail (bag != CORBA_OBJECT_NIL);

	bonobo_property_bag_client_set_value_string (bag, "url", url, &ev);

	gtk_box_pack_start (box, widget, TRUE, TRUE, 0);
	gtk_widget_show (widget);
} /* activate_clicked */
#else

#include <gtkhtml/gtkhtml.h>

static void
activate_clicked(GtkButton *button, GtkBox *box)
{
	CORBA_Environment ev;
	char *url;
	Bonobo_Stream stream;
	int bytes;
	char *buf;
	GtkWidget *frame;
	GtkWidget *html;
	GtkHTMLStream *hs;

	CORBA_exception_init(&ev);

	url = "http://localhost/index.html.gz!gunzip:";

	frame = gtk_scrolled_window_new(NULL, NULL);
	html = gtk_html_new();
	gtk_container_add(GTK_CONTAINER(frame), html);
	gtk_widget_show_all(frame);
	gtk_box_pack_start(box, frame, TRUE, TRUE, 0);

	stream = bonobo_get_object(url, "IDL:Bonobo/Stream:1.0", &ev);
	if (stream == CORBA_OBJECT_NIL) {
		g_warning("No stream");
		return;
	}
	hs = gtk_html_begin(GTK_HTML(html));
	
	do {
		bytes = bonobo_stream_client_read_string(
			stream, &buf, &ev);
		gtk_html_write(GTK_HTML(html), hs, buf, strlen(buf));
		g_free(buf);
	} while (bytes > 0);

	gtk_html_end(GTK_HTML(html), hs, GTK_HTML_STREAM_OK);
}
#endif

int
main (int argc, char *argv [])
{
	CORBA_Environment ev;
	CORBA_ORB  orb;
	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *button;

	CORBA_exception_init (&ev);

	gnome_init ("moniker test", "1.0", argc, (char **) argv);

	orb = oaf_init (argc, argv);

	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error ("Cannot init bonobo");

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER(window), vbox);
	button = gtk_button_new_with_label ("ACTIVATE!");
	gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
	gtk_signal_connect (
		GTK_OBJECT (button), "clicked",
		GTK_SIGNAL_FUNC (activate_clicked), vbox);
	gtk_widget_show_all (window);

	bonobo_activate ();
	gtk_main ();

	CORBA_exception_free (&ev);

	return 0;
}
