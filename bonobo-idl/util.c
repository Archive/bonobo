/*
 * util.c: Utility routines for the bonobo-idl compiler
 *
 * Author:
 *   Miguel de Icaza (miguel@helixcode.com)
 *
 * (C) 2000 Helix Code, Inc.
 */
#include "config.h"
#include <ctype.h>
#include <libIDL/IDL.h>
#include <glib.h>
#include "util.h"

/*
 * Taken from ORBit's IDL compiler
 */
static void
IDL_tree_traverse_helper(IDL_tree p, GFunc f,
			 gconstpointer func_data,
			 GHashTable *visited_nodes)
{
	IDL_tree curitem;

	if(g_hash_table_lookup(visited_nodes, p))
		return;

	g_hash_table_insert(visited_nodes, p, ((gpointer)1));

	for(curitem = IDL_INTERFACE(p).inheritance_spec; curitem;
	    curitem = IDL_LIST(curitem).next) {
		IDL_tree_traverse_helper(IDL_get_parent_node(IDL_LIST(curitem).data, IDLN_INTERFACE, NULL), f, func_data, visited_nodes);
	}

	f(p, (gpointer)func_data);
}

void
IDL_tree_traverse_parents(IDL_tree p,
			  GFunc f,
			  gconstpointer func_data)
{
	GHashTable *visited_nodes = g_hash_table_new(NULL, g_direct_equal);

	if(!(p && f))
		return;

	if(IDL_NODE_TYPE(p) != IDLN_INTERFACE)
		p = IDL_get_parent_node(p, IDLN_INTERFACE, NULL);

	if(!p)
		return;

	IDL_tree_traverse_helper(p, f, func_data, visited_nodes);

	g_hash_table_destroy(visited_nodes);
}

char *
lowercase (const char *str)
{
	char *copy = g_strdup (str);
	unsigned char *p;

	for (p = copy; *p; p++)
		if (isascii (*p))
			*p = tolower (*p);

	return copy;
}

char *
uppercase (const char *str)
{
	char *copy = g_strdup (str);
	unsigned char *p;

	for (p = copy; *p; p++)
		if (isascii (*p))
			*p = toupper (*p);

	return copy;
}

char *
trim_leading (char *p)
{
	while (*p == ' ' || *p == '\t' )
		p++;

	return g_strdup (p);
}

void
chop (char *buffer)
{
	char *p = buffer;
	
	while (*p){
		if (*p == '\n'){
			*p = 0;
			return;
		}
		p++;
	}
}

char *
join (char **array, const char *sep, int flags)
{
	int sep_len = strlen (sep);
	int len = 0, i;
	char *ret;
	
	for (i = 0; array [i] != NULL; i++)
		len += strlen (array [i]) + sep_len;
	len++;
	
	ret = g_new (char, len);
	ret [0] = 0;
	
	for (i = 0; array [i] != NULL; i++){
		if ((flags & UPPERCASE) != 0){
			char *p = uppercase (array [i]);

			strcat (ret, p);
			g_free (p);
		} else if ((flags & LOWERCASE) != 0){
			char *p = lowercase (array [i]);

			strcat (ret, p);
			g_free (p);
		} else
			strcat (ret, array [i]);

		if (array [i+1] != NULL)
			strcat (ret, sep);
	}

	return ret;
}

