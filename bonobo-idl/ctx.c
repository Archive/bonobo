/*
 * names.c: implements the name generation
 *
 * Author:
 *   Miguel de Icaza (miguel@kernel.org)
 *
 * (C) 2000 Helix Code, Inc.
 */
#include "config.h"
#include <stdio.h>
#include <errno.h>
#include <glib.h>
#include <string.h>
#include <libIDL/IDL.h>
#include "ctx.h"
#include "util.h"

const char *
name_gtk_parent_class (Ctx *ctx)
{
	return ctx->gtk_parent_class;
}

const char *
name_gtk_interface_base (Ctx *ctx)
{
	return ctx->gtk_interface_base;
}

const char *
name_corba_interface (Ctx *ctx)
{
	return ctx->corba_interface;
}

const char *
name_corba_interface_prefix (Ctx *ctx)
{
	return ctx->corba_interface_prefix;
}

const char *
name_gtk_object_type (Ctx *ctx)
{
	return ctx->gtk_object_type;
}

const char *
name_gtk_parent_class_base (Ctx *ctx)
{
	return ctx->gtk_parent_class;
}

const char *
name_gtk_parent_class_prefix (Ctx *ctx)
{
	return ctx->gtk_parent_class_prefix;
}

const char *
name_class_uppercase (Ctx *ctx)
{
	return ctx->class_uppercase;
}


static void
compute_parent (IDL_tree node, gpointer data)
{
	Ctx *ctx = data;
	
	if (ctx->gtk_parent_class)
		return;
	
	ctx->gtk_parent_class_prefix = lowercase (
		IDL_ns_ident_to_qstring (IDL_IDENT_TO_NS (IDL_INTERFACE (node).ident), "_", 0));

	ctx->gtk_parent_class =
		IDL_ns_ident_to_qstring (IDL_IDENT_TO_NS (IDL_INTERFACE (node).ident), "", 0);

	if (strcmp (ctx->gtk_parent_class_prefix, "bonobo_unknown") == 0){
		g_free (ctx->gtk_parent_class_prefix);
		g_free (ctx->gtk_parent_class);

		ctx->gtk_parent_class_prefix = g_strdup ("bonobo_object");
		ctx->gtk_parent_class = g_strdup ("BonoboObject");
	}

}

static char **
tokenize (const char *string)
{
	char *copy = g_strdup (string), *copy2;
	char **result, *p;
	int count;
	int max = 10;
	
	result = g_new (char *, max);

	copy2 = copy;
	for (count = 0; (p = strtok (copy, " \t")) != NULL; count++){
		if (count+1 == max-1){
			max += 10;
			result = g_renew (char *, result, max);
		}
		result [count] = g_strdup (p);
		copy = NULL;
	}
	g_free (copy2);
	result [count] = 0;

	return result;
}

static void
prefix_compute_names (Ctx *ctx)
{
	int i;
	char **names;
	
	names = tokenize (ctx->prefix);

	ctx->gtk_object_type = join (names, "", 0);
	ctx->gtk_interface_base = join (names, "_", LOWERCASE);
	ctx->class_uppercase = uppercase (ctx->gtk_interface_base);
	ctx->corba_interface_prefix = join (names, "_", 0);
}

void
ctx_compute_names (Ctx *ctx)
{
	ctx->corba_interface = IDL_ns_ident_to_qstring (IDL_IDENT_TO_NS (IDL_INTERFACE (ctx->base).ident), "_", 0);
	ctx->corba_interface_prefix = g_strdup (ctx->corba_interface);
	
	ctx->gtk_object_type = IDL_ns_ident_to_qstring (IDL_IDENT_TO_NS (IDL_INTERFACE (ctx->base).ident), "", 0);	
	ctx->gtk_interface_base = lowercase (ctx->corba_interface);

	IDL_tree_traverse_parents (ctx->base, (GFunc) compute_parent, ctx);
	ctx->class_uppercase = uppercase (ctx->gtk_interface_base);

	if (ctx->prefix){
		prefix_compute_names (ctx);
		return;
	}
}

Ctx *
ctx_new (const char *bcd_file)
{
	Ctx *ctx = g_new0 (Ctx, 1);

	ctx->bcd_file = g_strdup (bcd_file);
	
	return ctx;
}

void
ctx_free (Ctx *ctx)
{
	if (ctx->name)
		g_free (ctx->name);
	
	g_free (ctx);
}

