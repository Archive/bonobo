
typedef struct {
	IDL_tree base;
	char *bcd_file;
	char *name;
	
	FILE *output;

	/*
	 * Generated names
	 */
	char *gtk_interface_base;
	char *gtk_parent_class;
	char *corba_interface;
	char *corba_interface_prefix;
	char *gtk_object_type;

	char *gtk_parent_class_prefix;
	char *class_uppercase;

	/*
	 * Sections that we emit
	 */
	char *includes;
	char *object_data;
	char *class_data;
	char *protos;

	/*
	 * Overrides
	 */
	char *prefix;
} Ctx;

const char *name_gtk_parent_class        (Ctx *ctx);
const char *name_gtk_interface_base      (Ctx *ctx);
const char *name_corba_interface         (Ctx *ctx);
const char *name_corba_interface_prefix  (Ctx *ctx);
const char *name_gtk_object_type         (Ctx *ctx);
const char *name_gtk_parent_class_base   (Ctx *ctx);
const char *name_gtk_parent_class_prefix (Ctx *ctx);
const char *name_class_uppercase         (Ctx *ctx);

Ctx  *ctx_new           (const char *fname);
void  ctx_free          (Ctx *ctx);
void  ctx_compute_names (Ctx *ctx);

