
char *lowercase (const char *str);
char *uppercase (const char *str);

void IDL_tree_traverse_parents (IDL_tree p, GFunc f, gconstpointer func_data);

char *trim_leading (char *p);
void chop          (char *buffer);

#define UPPERCASE  1
#define LOWERCASE  2

char *join (char **array, const char *sep, int flags);
