/*
 * Sample: shows the implementation of test.bcd
 *
 */
#include "test.h"

static BonoboObjectClass *my_prefix_parent_class;

static void
impl_My_Prefix_quack (PortableServer_Servant servant, const CORBA_long frequency, CORBA_Environment *ev)
{
	MyPrefix *my_prefix = my_prefix_from_servant (servant);

	my_prefix->frequency = frequency;
	printf ("Quack for %d\n", frequency);
}

static CORBA_char *
impl_My_Prefix_get_message (PortableServer_Servant servant, CORBA_Environment *ev)
{
	MyPrefix *my_prefix = my_prefix_from_servant (servant);

	if (my_prefix->message)
		return CORBA_string_dup ("");
	else
		CORBA_string_dup (my_prefix->message);
}

static void
impl_My_Prefix_set_message (PortableServer_Servant servant, const CORBA_char *value, CORBA_Environment *ev)
{
	MyPrefix *my_prefix = my_prefix_from_servant (servant);

	if (my_prefix->message)
		g_free (my_prefix->message);

	my_prefix->message = g_strdup (value);
}

static void
my_prefix_destroy (GtkObject *object)
{
	MyPrefix *my_prefix = MY_PREFIX (object);

	if (my_prefix->message)
		g_free (my_prefix->message);
	
	GTK_OBJECT_CLASS (my_prefix_parent_class)->destroy (object);
}

static void
my_prefix_class_init (GtkObjectClass *object_class)
{
	object_class->destroy = my_prefix_destroy;

	my_prefix_parent_class = gtk_type_class (MY_PREFIX_PARENT_TYPE);
}

#include "test-bskel.c"
