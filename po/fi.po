# Bonobo Finnish translation
# Suomennos: http://gnome-fi.sourceforge.net/
#
# Copyright (C) 2001 Free Software Foundation, Inc.
# Sami Gerdt <sgerdt@cs.joensuu.fi>, 2000
# Pauli Virtanen <pauli.virtanen@saunalahti.fi>, 2000-2001
#
#
# embeddable = upote
# control = s��din
#  
msgid ""
msgstr ""
"Project-Id-Version: bonobo 0.10\n"
"POT-Creation-Date: 2002-10-06 14:37-0500\n"
"PO-Revision-Date: 2001-06-21 12:51+0300\n"
"Last-Translator: Pauli Virtanen <pauli.virtanen@saunalahti.fi>\n"
"Language-Team: Finnish <fi@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

#: bonobo/bonobo-ui-config-widget.c:274
msgid "Visible"
msgstr "N�kyv�"

#: bonobo/bonobo-ui-config-widget.c:282
msgid "_Show"
msgstr "_N�yt�"

#: bonobo/bonobo-ui-config-widget.c:292
msgid "_Hide"
msgstr "_Piilota"

#: bonobo/bonobo-ui-config-widget.c:302
msgid "_View tooltips"
msgstr "_N�yt� vinkit"

#: bonobo/bonobo-ui-config-widget.c:309
msgid "Toolbars"
msgstr "Ty�kalupalkit"

#: bonobo/bonobo-ui-config-widget.c:319 bonobo/bonobo-ui-sync-toolbar.c:518
msgid "Look"
msgstr "Katso"

#: bonobo/bonobo-ui-config-widget.c:329 bonobo/bonobo-ui-sync-toolbar.c:520
msgid "_Icon"
msgstr "_Kuvake"

#: bonobo/bonobo-ui-config-widget.c:339
msgid "_Text and Icon"
msgstr "_Teksti ja kuvake"

#: bonobo/bonobo-ui-config-widget.c:349
msgid "_Priority text only"
msgstr "_Vain t�rke� teksti"

#: bonobo/bonobo-ui-engine-config.c:524
msgid "Customize Toolbars"
msgstr "Mukauta ty�kalupalkkeja"

#: bonobo/bonobo-ui-sync-toolbar.c:519
msgid "B_oth"
msgstr "_Molemmat"

#: bonobo/bonobo-ui-sync-toolbar.c:521
msgid "T_ext"
msgstr "_Teksti"

#: bonobo/bonobo-ui-sync-toolbar.c:522
msgid "Hide t_ips"
msgstr "Piilota _vinkit"

#: bonobo/bonobo-ui-sync-toolbar.c:523
msgid "Show t_ips"
msgstr "N�yt� _vinkit"

#: bonobo/bonobo-ui-sync-toolbar.c:524
msgid "_Hide toolbar"
msgstr "_Piilota ty�kalupalkki"

#: bonobo/bonobo-ui-sync-toolbar.c:525
msgid "Customi_ze"
msgstr "Muka_uta"

#: bonobo/bonobo-ui-sync-toolbar.c:526
msgid "Customize the toolbar"
msgstr "Mukauta ty�kalupalkki"

#. -lefence
#: monikers/moniker-test.c:237
msgid "Oaf options"
msgstr "Oaf-asetukset"

#: samples/compound-doc/container/container.c:277 tests/test-ui.c:403
msgid "Could not initialize Bonobo!\n"
msgstr "Bonoboa ei voitu alustaa!\n"

#: bonobo/bonobo-exception.c:162
msgid "An unsupported action was attempted"
msgstr "Yritetty toimintoa, joka ei ole tuettu"

#: bonobo/bonobo-exception.c:165
msgid "IO Error"
msgstr "IO-virhe"

#: bonobo/bonobo-exception.c:168
msgid "Invalid argument value"
msgstr "Virheellinen parametrin arvo"

#: bonobo/bonobo-exception.c:172
msgid "Object not found"
msgstr "Oliota ei l�ytynyt"

#: bonobo/bonobo-exception.c:175
msgid "Syntax error in object description"
msgstr "Muotovirhe olion kuvauksessa"

#: bonobo/bonobo-exception.c:179
msgid "The User canceled the save"
msgstr "K�ytt�j� peruutti tallentamisen"

#: bonobo/bonobo-exception.c:184
msgid "Cannot activate object from factory"
msgstr "Oliota ei voi aktivoida tehtaasta"

#: bonobo/bonobo-exception.c:189
msgid "No permission to access stream"
msgstr "Ei oikeuksia virran k�ytt��n"

#: bonobo/bonobo-exception.c:192
msgid "An unsupported stream action was attempted"
msgstr "Yritettiin virtatoimintoa, joka ei ole tuettu"

#: bonobo/bonobo-exception.c:195
msgid "IO Error on stream"
msgstr "IO-virhe virrassa"

#: bonobo/bonobo-exception.c:199
msgid "IO Error on storage"
msgstr "IO-virhe varastossa"

#: bonobo/bonobo-exception.c:202
msgid "Name already exists in storage"
msgstr "Nimi on jo varastossa"

#: bonobo/bonobo-exception.c:205
msgid "Object not found in storage"
msgstr "Olio ei ollut varastossa"

#: bonobo/bonobo-exception.c:208
msgid "No permission to do operation on storage"
msgstr "Ei lupaa varaston k�sittelyyn"

#: bonobo/bonobo-exception.c:210
msgid "An unsupported storage action was attempted"
msgstr "Yritettiin varastotoimintoa, joka ei ole tuettu"

#: bonobo/bonobo-exception.c:212
msgid "Object is not a stream"
msgstr "Olio ei ole virta"

#: bonobo/bonobo-exception.c:215
msgid "Object is not a storage"
msgstr "Olio ei ole varasto"

#: bonobo/bonobo-exception.c:218
msgid "Storage is not empty"
msgstr "Varasto ei ole tyhj�"

#: bonobo/bonobo-exception.c:222
msgid "malformed user interface XML description"
msgstr "viallinen k�ytt�liittym�n XML-kuvaus"

#: bonobo/bonobo-exception.c:225
msgid "invalid path to XML user interface element"
msgstr "virheellinen polku XML-k�ytt�liittym�osaan"

#: bonobo/bonobo-exception.c:229
msgid "incorrect data type"
msgstr "v��r� tietotyyppi"

#: bonobo/bonobo-exception.c:232
msgid "stream not found"
msgstr "virtaa ei l�ydy"

#: bonobo/bonobo-exception.c:236
msgid "property not found"
msgstr "ominaisuutta ei l�ydy"

#: bonobo/bonobo-exception.c:240
msgid "Moniker interface cannot be found"
msgstr "Moniker-rajapintaa ei l�ydy"

#: bonobo/bonobo-exception.c:243
msgid "Moniker activation timed out"
msgstr "Monikerin aktivointi aikakatkaistu"

#: bonobo/bonobo-exception.c:246
msgid "Syntax error within moniker"
msgstr "Monikerissa muotovirhe"

#: bonobo/bonobo-exception.c:249
msgid "Moniker has an unknown moniker prefix"
msgstr "Monikerilla on tuntematon moniker-etuliite"

#: bonobo/bonobo-exception.c:273
msgid "Error checking error; no exception"
msgstr "Virhe virhett� tarkistaessa; ei poikkeusta"

#: bonobo/bonobo-object-directory.c:225
msgid "No filename"
msgstr "Ei tiedostonnime�"

#: bonobo/bonobo-object-directory.c:231
#, c-format
msgid "unknown mime type for '%s'"
msgstr "tiedoston '%s' mime-tyyppi� ei tunneta"

#: bonobo/bonobo-object-directory.c:256
#, c-format
msgid "no handlers for mime type '%s'"
msgstr "mime-tyypille '%s' ei ole k�sittelij�it�"

#: bonobo/bonobo-selector-widget.c:213
msgid "Name"
msgstr "Nimi"

#: bonobo/bonobo-selector-widget.c:236
msgid "Description"
msgstr "Kuvaus"

#: components/application-x-gnomine/Bonobo_Sample_Mines.oaf.in.h:1
msgid "GnoMines control"
msgstr "GnoMines-s��din"

#: components/application-x-gnomine/Bonobo_Sample_Mines.oaf.in.h:2
msgid "GnoMines control factory"
msgstr "GnoMines-s��dintehdas"

#: components/application-x-gnomine/Bonobo_Sample_Mines.oaf.in.h:3
msgid "bonobo GnoMines object"
msgstr "bonobo GnoMines -olio"

#: components/application-x-gnomine/Bonobo_Sample_Mines.oaf.in.h:4
msgid "bonobo GnoMines object factory"
msgstr "bonobo Gnomines -oliotehdas"

#: components/application-x-gnomine/bonobo-application-x-mines-ui.xml.h:1
msgid "Load a saved game"
msgstr "Lataa tallennettu peli"

#: components/application-x-gnomine/bonobo-application-x-mines-ui.xml.h:2
msgid "New game"
msgstr "Uusi peli"

#: components/application-x-gnomine/bonobo-application-x-mines-ui.xml.h:3
msgid "Open game"
msgstr "Avaa peli"

#: components/application-x-gnomine/bonobo-application-x-mines-ui.xml.h:4
msgid "Start a new game"
msgstr "Aloita uusi peli"

#: components/application-x-gnomine/bonobo-application-x-mines-ui.xml.h:5
msgid "_Game"
msgstr "_Peli"

#: components/audio-ulaw/Bonobo_Sample_Audio.oaf.in.h:1
msgid "Waveform"
msgstr "Aalto��ni"

#: components/audio-ulaw/Bonobo_Sample_Audio.oaf.in.h:2
msgid "Waveform component"
msgstr "Aalto��nikomponentti"

#: components/audio-ulaw/Bonobo_Sample_Audio.oaf.in.h:3
msgid "Waveform component factory"
msgstr "Aalto��nikomponenttitehdas"

#: components/audio-ulaw/Bonobo_Sample_Audio.oaf.in.h:4
msgid "audio/ulaw bonobo object"
msgstr "audio/ulaw bonobo-olio"

#: components/audio-ulaw/Bonobo_Sample_Audio.oaf.in.h:5
msgid "audio/ulaw bonobo object factory"
msgstr "audio/ulaw bonobo-oliotehdas"

#: components/selector/Bonobo_Selector.oaf.in.h:1
msgid "Component Selector"
msgstr "Komponenttivalitsin"

#: components/selector/Bonobo_Selector.oaf.in.h:2
msgid "Component Selector factory"
msgstr "Komponenttivalitsintehdas"

#: components/selector/Bonobo_Selector.oaf.in.h:3
msgid "Component selector control"
msgstr "Komponenttivalitsins��din"

#: components/selector/Bonobo_Selector.oaf.in.h:4
msgid "component selector control factory"
msgstr "komponenttivalitsins��dintehdas"

#: components/text-plain/Bonobo_Sample_Text.oaf.in.h:1
msgid "Text"
msgstr "Teksti"

#: components/text-plain/Bonobo_Sample_Text.oaf.in.h:2
msgid "Text component"
msgstr "Tekstikomponentti"

#: components/text-plain/Bonobo_Sample_Text.oaf.in.h:3
msgid "Text component factory"
msgstr "Tekstikomponenttitehdas"

#: components/text-plain/Bonobo_Sample_Text.oaf.in.h:4
msgid "text/plain component"
msgstr "text/plain-komponentti"

#: components/text-plain/Bonobo_Sample_Text.oaf.in.h:5
msgid "text/plain embeddable factory"
msgstr "text/plain-upotetehdas"

#: components/text-plain/bonobo-text-plain.c:338
msgid "_Clear Text"
msgstr "_Tyhjenn� teksti"

#: components/text-plain/bonobo-text-plain.c:339
msgid "Clears the text in the component"
msgstr "Tyhjent�� komponentin tekstin"

#: gshell/controls.c:95 gshell/inout.c:91
msgid ""
"An exception occured while trying to load data into the component with "
"PersistFile"
msgstr ""
"Poikkeus tapahtui yritt�ess� ladata dataa PersistFilell� komponenttiin."

#: gshell/controls.c:119 gshell/inout.c:176
#: samples/compound-doc/container/component.c:228
#, c-format
msgid "Could not open file %s"
msgstr "Tiedostoa %s ei voitu avata"

#: gshell/controls.c:144 gshell/inout.c:201
msgid ""
"An exception occured while trying to load data into the component with "
"PersistStream"
msgstr ""
"Poikkeus tapahtui yritt�ess� ladata dataa PersistStreamilla komponenttiin."

#: gshell/gshell-ui.xml.h:1
msgid "About this application"
msgstr "Tietoja t�st� ohjelmasta"

#: gshell/gshell-ui.xml.h:2
msgid "Close the current window"
msgstr "Sulje nykyinen ikkuna"

#: gshell/gshell-ui.xml.h:3
msgid "Configure the application"
msgstr "Muuta ohjelman asetuksia"

#: gshell/gshell-ui.xml.h:4
msgid "Create New _Window"
msgstr "Luo uusi _ikkuna"

#: gshell/gshell-ui.xml.h:5
msgid "Create a new window"
msgstr "Luo uusi ikkuna"

#: gshell/gshell-ui.xml.h:6
msgid "Displays only one window"
msgstr "N�ytt�� vain yhden ikkunan"

#: gshell/gshell-ui.xml.h:7
msgid "E_xit"
msgstr "_Lopeta"

#: gshell/gshell-ui.xml.h:8
msgid "Exit the program"
msgstr "Lopeta ohjelma"

#: gshell/gshell-ui.xml.h:9
msgid "Help on GNOME"
msgstr "Gnomen ohje"

#: gshell/gshell-ui.xml.h:10
msgid "Kill the current buffer"
msgstr "Sulje nykyinen puskuri"

#: gshell/gshell-ui.xml.h:11
msgid "Launch Control..."
msgstr "K�ynnist� s��din..."

#: gshell/gshell-ui.xml.h:12
msgid "Launch Embeddable..."
msgstr "K�ynnist� upote..."

#: gshell/gshell-ui.xml.h:13
msgid "Launches a new Control component"
msgstr "K�ynnist�� uuden s��dinkomponentin"

#: gshell/gshell-ui.xml.h:14
msgid "Launches a new Embeddable component"
msgstr "K�ynnist� uusi upotekomponentti"

#: gshell/gshell-ui.xml.h:15
msgid "Loads a file into the current component"
msgstr "Lataa tiedoston nykyiseen komponenttiin"

#: gshell/gshell-ui.xml.h:16
msgid "Open a file"
msgstr "Avaa tiedosto"

#: gshell/gshell-ui.xml.h:17
msgid "Print S_etup..."
msgstr "Tul_ostimen asetukset..."

#: gshell/gshell-ui.xml.h:18
msgid "Print the current file"
msgstr "Tulosta nykyinen tiedosto"

#: gshell/gshell-ui.xml.h:19
msgid "Save _As..."
msgstr "Tallenna _nimell�..."

#: gshell/gshell-ui.xml.h:20
msgid "Save the current file"
msgstr "Tallenna nykyinen tiedosto"

#: gshell/gshell-ui.xml.h:21
msgid "Save the current file with a different name"
msgstr "Tallenna nykyinen tiedosto eri nimell�"

#: gshell/gshell-ui.xml.h:22
msgid "Setup the page settings for your current printer"
msgstr "Aseta tulostimen sivun asetukset"

#: gshell/gshell-ui.xml.h:23
msgid "Splits the current window"
msgstr "Jaa ikkuna kahtia"

#: gshell/gshell-ui.xml.h:24
msgid "Zoom ..."
msgstr "Suurenna..."

#: gshell/gshell-ui.xml.h:25
msgid "Zoom _in"
msgstr "_Suurenna"

#: gshell/gshell-ui.xml.h:26
msgid "Zoom _out"
msgstr "_Pienenn�"

#: gshell/gshell-ui.xml.h:27
msgid "Zoom to _default"
msgstr "_Oletussuurennos"

#: gshell/gshell-ui.xml.h:28
msgid "Zoom to _fit"
msgstr "So_vita"

#: gshell/gshell-ui.xml.h:29
msgid "_About..."
msgstr "_Tietoja..."

#: gshell/gshell-ui.xml.h:30
msgid "_Buffers"
msgstr "_Puskurit"

#: gshell/gshell-ui.xml.h:31
msgid "_Close This Window"
msgstr "_Sulje t�m� ikkuna"

#: gshell/gshell-ui.xml.h:32
msgid "_File"
msgstr "_Tiedosto"

#: gshell/gshell-ui.xml.h:33
msgid "_Help"
msgstr "_Ohje"

#: gshell/gshell-ui.xml.h:34
msgid "_Help on GNOME"
msgstr "_Gnomen ohje"

#: gshell/gshell-ui.xml.h:35
msgid "_Kill"
msgstr "_Sulje"

#: gshell/gshell-ui.xml.h:36
msgid "_Load..."
msgstr "_Lataa..."

#: gshell/gshell-ui.xml.h:37
msgid "_One Window"
msgstr "_Yksi ikkuna"

#: gshell/gshell-ui.xml.h:38
msgid "_Open..."
msgstr "_Avaa..."

#: gshell/gshell-ui.xml.h:39
msgid "_Preferences..."
msgstr "_Asetukset..."

#: gshell/gshell-ui.xml.h:40
msgid "_Print"
msgstr "_Tulosta"

#: gshell/gshell-ui.xml.h:41
msgid "_Save"
msgstr "_Tallenna"

#: gshell/gshell-ui.xml.h:42
msgid "_Settings"
msgstr "_Asetukset"

#: gshell/gshell-ui.xml.h:43
msgid "_Split Window"
msgstr "_Jaa ikkuna"

#: gshell/gshell-ui.xml.h:44
msgid "_Window"
msgstr "_Ikkuna"

#: gshell/gshell-ui.xml.h:45
msgid "_Zoom"
msgstr "_Suurenna"

#: gshell/gshell.c:46
msgid ""
"This program is part of the GNOME project. Gnome Shell comes with ABSOLUTELY "
"NO WARRANTY. This is free software, and you are welcome to redistribute it "
"under the conditions of the GNU General Public License. Please report bugs "
"to dietmar@maurer-it.com"
msgstr ""
"T�m� ohjelma on osa GNOME-projektia. Gnome Shellill� ei ole MINK��NLAISTA "
"TAKUUTA. T�m� on vapaata ohjelmistoa, ja voit levitt�� sit� vapaasti GNU "
"General Public Licensen ehtojen mukaan. Raportoi virheist� (englanniksi) "
"s�hk�postiosoitteeseen dietmar@maurer-it.com"

#: gshell/gshell.c:89
msgid "fatal CORBA exception!  Shutting down..."
msgstr "vakava CORBA-poikkeus!  Sammutaan..."

#: gshell/gshell.c:162
#, c-format
msgid "New zoom level is %.3g."
msgstr "Uusi suurennostaso on %.3g"

#: gshell/gshell.c:596
msgid "Set zoom level:"
msgstr "Aseta suurennos:"

#: gshell/gshell.c:652
msgid "Could not initialize Bonobo!"
msgstr "Bonoboa ei voitu alustaa!"

#: gshell/inout.c:55
msgid ""
"An exception occured while trying to save data from the component with "
"PersistFile"
msgstr ""
"Poikkeus tapahtui yritt�ess� tallentaa dataa PersistFilell� komponentista."

#: gshell/inout.c:115
#, c-format
msgid "Could not save file %s"
msgstr "Tiedostoa %s ei voitu tallentaa"

#: gshell/inout.c:151
msgid ""
"An exception occured while trying to save data from the component with "
"PersistStream"
msgstr ""
"Poikkeus tapahtui yritt�ess� tallentaa dataa PersistStreamilla komponentista."

#: gshell/inout.c:364
msgid "Open"
msgstr "Avaa"

#: gshell/inout.c:393 gshell/inout.c:450
msgid "Launch component"
msgstr "K�ynnist�� komponentti"

#: gshell/inout.c:553
msgid "Load"
msgstr "Lataa"

#: gshell/inout.c:604
msgid "Save As"
msgstr "Tallenna nimell�"

#: gshell/properties.c:152
msgid "Component has no editable properties"
msgstr "Komponentilla ei ole muokattavia ominaisuuksia"

#: monikers/Bonobo_Moniker_gzip.oaf.in.h:1
msgid "Gunzip Moniker factory"
msgstr "Gunzip-monikertehdas"

#: monikers/Bonobo_Moniker_gzip.oaf.in.h:2
msgid "gunzip Moniker"
msgstr "gunzip-moniker"

#: monikers/Bonobo_Moniker_std.oaf.in.in.h:1
msgid "HTTP Moniker"
msgstr "HTTP-moniker"

#: monikers/Bonobo_Moniker_std.oaf.in.in.h:2
msgid "Standard Moniker factory"
msgstr "Tavallisten Monikereiden tehdas"

#: monikers/Bonobo_Moniker_std.oaf.in.in.h:3
msgid "file MonikerExtender"
msgstr "tiedosto-MonikerExtender"

#: monikers/Bonobo_Moniker_std.oaf.in.in.h:4
msgid "generic Oaf activation moniker"
msgstr "yleinen Oaf-aktivointimoniker"

#: monikers/Bonobo_Moniker_std.oaf.in.in.h:5
msgid "generic Oaf query moniker"
msgstr "yleinen Oaf-kyselymoniker"

#: monikers/Bonobo_Moniker_std.oaf.in.in.h:6
msgid "generic factory 'new' moniker"
msgstr "yleinen tehdas 'new'-moniker"

#: monikers/Bonobo_Moniker_std.oaf.in.in.h:7
msgid "generic file moniker"
msgstr "yleinen tiedostomoniker"

#: monikers/Bonobo_Moniker_std.oaf.in.in.h:8
msgid "generic item moniker"
msgstr "yleinen kohdemoniker"

#: monikers/Bonobo_Moniker_std.oaf.in.in.h:9
msgid "stream MonikerExtender"
msgstr "virta-MonikerExtender"

#: samples/bonobo-class/Bonobo_Sample_Echo.oaf.in.h:1
msgid "Bonobo Echo server factory"
msgstr "Bonobo-kaikupalvelintehdas"

#: samples/bonobo-class/Bonobo_Sample_Echo.oaf.in.h:2
msgid "Bonobo Echo server sample program"
msgstr "Bonobo-kaikupalvelinesimerkkiohjelma"

#: samples/bonobo-class/Bonobo_Sample_Echo.oaf.in.h:3
msgid "Echo component"
msgstr "Kaikukomponentti"

#: samples/bonobo-class/Bonobo_Sample_Echo.oaf.in.h:4
msgid "Echo component factory"
msgstr "Kaikukomponenttitehdas"

#: samples/bonobo-class/echo-client.c:30
msgid "I could not initialize Bonobo"
msgstr "Bonoboa ei voitu alustaa"

#: samples/bonobo-class/main.c:40
msgid "Could not initialize Bonobo"
msgstr "Ei voinut alustaa Bonoboa"

#: samples/compound-doc/container/component.c:253
msgid "The component now claims that it doesn't support PersistStream!"
msgstr "Komponentti v�itt��, ettei se tue PersistStreamia!"

#: samples/compound-doc/container/container-filesel.c:18
msgid "Select file"
msgstr "Valitse tiedosto"

#: samples/compound-doc/container/container-menu.c:18
msgid "Select an embeddable Bonobo component to add"
msgstr "Valitse lis�tt�v� upotekomponentti"

#: samples/compound-doc/container/container-menu.c:100
msgid "Bonobo sample container"
msgstr "Bonobon esimerkkis�il�"

#: samples/compound-doc/container/container.c:45
msgid "Sample Bonobo container"
msgstr "Bonobon esimerkkis�il�"

#: samples/compound-doc/container/container.c:113
#, c-format
msgid "Could not launch Embeddable %s!"
msgstr "Upotetta %s ei voitu k�ynnist��!"

#: samples/compound-doc/container/embeddable-io.c:36
msgid ""
"An exception occured while trying to load data into the component with "
"PersistStorage"
msgstr ""
"Poikkeus tapahtui yritt�ess� ladata dataa PersistStoragella komponenttiin."

#: samples/compound-doc/container/embeddable-io.c:67
msgid ""
"An exception occured while trying to save data from the component with "
"PersistStorage"
msgstr ""
"Poikkeus tapahtui yritt�ess� tallentaa dataa PersistStoragella komponentista."

#: tests/selector_test.c:22 tests/selector_test.c:46
msgid "Select an object"
msgstr "Valitse olio"

#: tests/test-storage.c:489
msgid "Can not bonobo_init"
msgstr "Ei voi suorittaa bonobo_init"
