
#include <libIDL/IDL.h>
#include <stdio.h>
#include <glib.h>
#include <errno.h>
/*
 * Compile with: gcc -o idl-grep idl-grep.c -lIDL `glib-config --cflags --libs`
 * */

enum {
	INTERFACES,
	MODULES,
	EXCEPTIONS,
	ATTRIBUTES,
	OPERATIONS,
	CONSTANTS,
	TYPES,
	ENUMS,
	STRUCTS,
	UNIONS
};

/* must match the enum above */
static const char * names[] = {
	"Interface",
	"Module",
	"Exception",
	"Attribute",
	"Operation",
	"Constant",
	"Type",
	"Enum",
	"Struct",
	"Union",
	NULL
};

static GHashTable *seen = NULL;
static int selector = 0;

static void 
output (int type, char *file, IDL_tree ident) {
	char * repoid;
	char *sf;
	
	if (!(selector & (1<<type)))
		return;
	
	if (IDL_NODE_TYPE(ident) == IDLN_TYPE_ARRAY)
		repoid = IDL_IDENT_REPO_ID(IDL_TYPE_ARRAY (ident).ident);
	else
		repoid = IDL_IDENT_REPO_ID (ident);
	
	if ((sf=g_hash_table_lookup(seen, repoid))) {
		if (strcmp(sf, file))
			g_warning("RepoID %s defined in both %s and %s", repoid, sf, file);
		return;
	}
	g_print("%s: %s: %s\n", file, names[type], repoid);
	g_hash_table_insert(seen, g_strdup(repoid), g_strdup(file));
}

static gboolean
tree_func (IDL_tree_func_data * idl, gpointer user_data) {
	char *file = idl->tree->_file;
	
    switch (IDL_NODE_TYPE (idl->tree)) {
	case IDLN_INTERFACE:
		output(INTERFACES, file, IDL_INTERFACE (idl->tree).ident);
		break;
	case IDLN_MODULE:
		output(MODULES, file, IDL_MODULE (idl->tree).ident);
		break;
	case IDLN_TYPE_ENUM:
		output(ENUMS, file, IDL_TYPE_ENUM (idl->tree).ident);
		break;
	case IDLN_TYPE_STRUCT:
		output(STRUCTS, file, IDL_TYPE_STRUCT (idl->tree).ident);
		break;
	case IDLN_TYPE_UNION:
		output(UNIONS, file, IDL_TYPE_UNION (idl->tree).ident);
		break;
	case IDLN_EXCEPT_DCL:
		output(EXCEPTIONS, file, IDL_EXCEPT_DCL (idl->tree).ident);
		break;
	case IDLN_OP_DCL:
		output(OPERATIONS, file, IDL_OP_DCL (idl->tree).ident);
		break;
	case IDLN_CONST_DCL:
		output(CONSTANTS, file, IDL_CONST_DCL (idl->tree).ident);
		break;
	case IDLN_ATTR_DCL: {
		IDL_tree simple_d = IDL_ATTR_DCL (idl->tree).simple_declarations;
		while (simple_d) {
			output(ATTRIBUTES, file, IDL_LIST (simple_d).data);
			simple_d = IDL_LIST(simple_d).next;
		}
		break;
	}
	case IDLN_TYPE_DCL: {
		IDL_tree dcls = IDL_TYPE_DCL (idl->tree).dcls;
		while (dcls) {
			output(TYPES, file, IDL_LIST (dcls).data);
			dcls = IDL_LIST(dcls).next;
		}
		break;
	}
	default:
		break;
	} /* switch */
	return TRUE;
}

void usage(int code) {
	g_print("Usage: idl-grep [--cpp flags] [--all | selectors] filename [...]\n"
			"Grep for repository IDs in idl files.\n"
			"Selectors can be:\n"
			"\t--interfaces -i\n"
			"\t--modules -m\n"
			"\t--exceptions -E\n"
			"\t--attributes -a\n"
			"\t--operations -m\n"
			"\t--constants -c\n"
			"\t--types -t\n"
			"\t--enums -e\n"
			"\t--structs -s\n"
			"\t--unions -u\n"
		);
	exit(code);
}

int 
main(int argc, char* argv[]) {
	
	char *file = NULL;
	char *cpp_args = "-D__ORBIT_IDL__";
	IDL_tree tree;
	IDL_ns ns;
	int ret;
	int i;

	if (argc < 2)
		usage(1);

	seen = g_hash_table_new(g_str_hash, g_str_equal);

	/* parse options */
	for (i=1; argv[i]; ++i) {
		if (!strcmp(argv[i], "--cpp")) {
			i++;
			if (!argv[i]) usage(1);
			cpp_args = argv[i];
		} else if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h")) {
			usage(0);
		} else if (!strcmp(argv[i], "--interfaces") || !strcmp(argv[i], "-i")) {
			selector |= 1<<INTERFACES;
		} else if (!strcmp(argv[i], "--modules") || !strcmp(argv[i], "-m")) {
			selector |= 1<<MODULES;
		} else if (!strcmp(argv[i], "--exceptions") || !strcmp(argv[i], "-E")) {
			selector |= 1<<EXCEPTIONS;
		} else if (!strcmp(argv[i], "--attributes") || !strcmp(argv[i], "-a")) {
			selector |= 1<<ATTRIBUTES;
		} else if (!strcmp(argv[i], "--operations") || !strcmp(argv[i], "-o")) {
			selector |= 1<<OPERATIONS;
		} else if (!strcmp(argv[i], "--constants") || !strcmp(argv[i], "-c")) {
			selector |= 1<<CONSTANTS;
		} else if (!strcmp(argv[i], "--types") || !strcmp(argv[i], "-t")) {
			selector |= 1<<TYPES;
		} else if (!strcmp(argv[i], "--enums") || !strcmp(argv[i], "-e")) {
			selector |= 1<<ENUMS;
		} else if (!strcmp(argv[i], "--structs") || !strcmp(argv[i], "-s")) {
			selector |= 1<<STRUCTS;
		} else if (!strcmp(argv[i], "--unions") || !strcmp(argv[i], "-u")) {
			selector |= 1<<UNIONS;
		} else if (!strcmp(argv[i], "--all")) {
			selector |= 0xffff;
		} else {
			break;
		}
	}

	for (; argv[i]; ++i) {
		file = argv[i];

		ret = IDL_parse_filename (file, cpp_args, NULL, &tree, &ns,
			IDLF_TYPECODES | IDLF_CODEFRAGS, IDL_WARNING1);
		if (ret == IDL_ERROR) {
			fprintf(stderr, "Error parsing IDL file %s\n", file);
			return 1;
		} else if (ret < 0) {
			fprintf(stderr, "System error parsing IDL file %s: %s\n", file, g_strerror(errno));
			return 2;
		}
		IDL_tree_walk_in_order (tree, tree_func, NULL);
		IDL_tree_free (tree);
		IDL_ns_free (ns);
	}
	return 0;
}

