/*--------------------------------*-C-*---------------------------------*
 *
 *  Copyright 1999, Matt Loper <matt@gnome-support.com>.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 *
 *----------------------------------------------------------------------*/

#ifndef BONOBROWSER_H
#define BONOBROWSER_H

#include <config.h>

/* There are three pages in our notebook, each
 * of which has a creation routine that returns
 * the widget that is put inside that notebook */

/* the idl browser page */
GtkWidget* idl_browser_new (void);
GtkWidget* idl_browser_new_with_filenames (int argc, char *argv[]);

/* the nameservice browser page */
GtkWidget* nameservice_browser_new (void);

/* the registration browser page */
GtkWidget* object_browser_new (void);
GtkWidget* object_browser_register_new_object(void);

/* each tree control should start out with similar dimensions */
#define TREECTRL_HEIGHT 300
#define TREECTRL_WIDTH 350

#endif // BONOBROWSER_H
